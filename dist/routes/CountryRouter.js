"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const CountryDAO_1 = require("../dao/CountryDAO");
const Country_1 = require("../model/Country");
class CountryRouter {
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    getAll(req, res, next) {
        let countryDAO = new CountryDAO_1.CountryDAO();
        let countryList = null;
        if (req.query.withCapital == '1') {
            countryList = countryDAO.getAllWithCapital();
        }
        else {
            countryList = countryDAO.getAll();
        }
        if (countryList) {
            res.status(200).send(JSON.stringify(countryList));
        }
        else {
            res.status(404).send('nix gefunden');
        }
    }
    foo(req, res, next) {
        res.send("fooBar");
    }
    getOne(req, res, next) {
        let countryDAO = new CountryDAO_1.CountryDAO();
        let country = null;
        country = countryDAO.getById(req.params.codeParam);
        if (country) {
            res.status(200).send(JSON.stringify(country));
        }
        else {
            res.status(404).send('nix gefunden');
        }
    }
    save(req, res, next) {
        let countryDAO = new CountryDAO_1.CountryDAO();
        let country = new Country_1.Country();
        console.log(req.body);
        country.code = req.body.code;
        country.name = req.body.name;
        country.population = req.body.population;
        country.continent = req.body.continent;
        // countryDAO.save(country);
        res.send("angekommen --> " + JSON.stringify(country));
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/', this.getAll);
        this.router.post('/', this.save);
        this.router.get('/foo', this.foo);
        this.router.get('/:codeParam', this.getOne);
    }
}
exports.CountryRouter = CountryRouter;
const countryRoutes = new CountryRouter();
exports.default = countryRoutes.router;
