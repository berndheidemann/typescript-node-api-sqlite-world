"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class City {
    constructor(id, name, population) {
        this.id = id;
        this.name = name;
        this.population = population;
    }
}
exports.City = City;
