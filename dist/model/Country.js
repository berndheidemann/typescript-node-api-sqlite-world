"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Country {
    constructor(code, name, continent, population, surfaceArea, capital) {
        this.code = code;
        this.name = name;
        this.continent = continent;
        this.population = population;
        this.surfaceArea = surfaceArea;
        this.capital = capital;
    }
}
exports.Country = Country;
