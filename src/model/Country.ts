import { City } from './City';
export class Country {

    constructor(public code?: string, public name?: string, public continent?: string, public population?: number, public surfaceArea?: number, public capital?: City) {

    }




}