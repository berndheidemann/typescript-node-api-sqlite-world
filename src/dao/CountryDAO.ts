import { Country } from '../model/Country';
import { City } from '../model/City';
import Sqlite = require('sqlite-sync');

export class CountryDAO {

    private DB_FILE = 'World.db3';

    save(country: Country) {

        // Sqlite.connect(this.DB_FILE);
        // Sqlite.run(`insert into Country (code, name, continent, region, surfaceArea, population) VALUES ('${country.code}', '${country.name}', '${country.continent}', 'foo', 123, 123)`, e => console.log(e));
        // Sqlite.close();


    }

    getAll(): Country[] {
        let countryList: Country[] = [];

        Sqlite.connect(this.DB_FILE);

        Sqlite.run("SELECT * from Country").forEach(result => {
            countryList.push(new Country(result.Code, result.Name, result.Continent, result.Population, result.SurfaceArea));
        });

        Sqlite.close();
        return countryList;
    }

    getAllWithCapital(): Country[] {
        let countryList: Country[] = [];

        Sqlite.connect(this.DB_FILE);
        // console.log("hier: " + JSON.stringify(Sqlite.run("SELECT * from Country co join City ci on co.capital = ci.id where ci.name = 'Berlin'")));
        Sqlite.run(`SELECT 
                        co.Code,
                        co.name as CountryName,
                        co.continent,
                        co.Population,
                        co.SurfaceArea,
                        ci.name as CityName,
                        ci.ID,
                        ci.Population as CityPopulation
                    from Country join City on capital = id`).forEach(result => {
                const city: City = new City(result.ID, result.CityName, result.CityPopulation);
                countryList.push(new Country(result.Code, result.CountryName, result.Continent, result.Population, result.SurfaceArea, city));
            });

        Sqlite.close();
        return countryList;

    }

    getById(code: string): Country {
        Sqlite.connect(this.DB_FILE);
        let country: Country;
        Sqlite.run(`SELECT * from Country where Code = '${code}'`).forEach(result => {
            console.log("erg: " + JSON.stringify(result));
            if (result) {
                country = new Country(result.Code, result.Name, result.Continent, result.Population, result.SurfaceArea);
            }
        });

        Sqlite.close();
        return country;
    }


}