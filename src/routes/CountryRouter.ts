import { Router, Request, Response, NextFunction } from 'express';
import { CountryDAO } from '../dao/CountryDAO';
import { Country } from '../model/Country';

export class CountryRouter {
  router: Router

  constructor() {
    this.router = Router();
    this.init();
  }


  public getAll(req: Request, res: Response, next: NextFunction) {
    let countryDAO = new CountryDAO();
    let countryList = null;

    if (req.query.withCapital == '1') {
      countryList = countryDAO.getAllWithCapital();
    } else {

      countryList = countryDAO.getAll();
    }
    if (countryList) {
      res.status(200).send(JSON.stringify(countryList));
    }
    else {
      res.status(404).send('nix gefunden');
    }

  }

  public foo(req: Request, res: Response, next: NextFunction) {
    res.send("fooBar");

  }


  public getOne(req: Request, res: Response, next: NextFunction) {
    let countryDAO = new CountryDAO();
    let country: Country = null;

    country = countryDAO.getById(req.params.codeParam);

    if (country) {
      res.status(200).send(JSON.stringify(country));
    }
    else {
      res.status(404).send('nix gefunden');
    }
  }

  public save(req: Request, res: Response, next: NextFunction) {
    let countryDAO = new CountryDAO();
    let country: Country = new Country();
    console.log(req.body);
    country.code = req.body.code;
    country.name = req.body.name;
    country.population = req.body.population;
    country.continent = req.body.continent;
    // countryDAO.save(country);

    res.send("angekommen --> " + JSON.stringify(country));
  }


  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.get('/', this.getAll);
    this.router.post('/', this.save)
    this.router.get('/foo', this.foo);
    this.router.get('/:codeParam', this.getOne);
  }

}

const countryRoutes = new CountryRouter();
export default countryRoutes.router;
